# A Guide on how to use the Oculus Rift with OpenHMD & SteamVR

*Last Updated: January 16 2023*

I just want to preface this by saying the Oculus Rift gaming experience with OpenHMD is far from perfect (**Tracking issues**, **Flickering**, **Weird issues in certain games**, and **more**). Windows is still the best way to play VR at the moment.

This guide is inspired a great deal by [this guide](https://noraisin.net/diary/?page_id=1048)

---

## Few notes before starting

- This method was confirmed working on **Wayland** ( KDE ✅, Gnome is Untested ) and **X11** ( KDE ✅, Gnome is Untested )
- Seems to only be working the the non-Flatpak version of Steam at the moment
- Inside SteamVR, Desktop View doesn't seem to work (Both **Wayland** and **X11**)
- The sensors **AND** the headset **NEED** to be plugged into USB 3.0 (and up) ports
- There is an issue with newer Nvidia drivers that disables output to the headset. A workaround is to downgrade the drivers. [More information](https://github.com/OpenHMD/OpenHMD/issues/340#issuecomment-1193042375)

### Weird issues I've encountered
- Opening SteamVR without the headset on your head in a way that it detects you, **hard crashes the Mesa drivers** (KDE Wayland, Mesa version 22.3.3)
- Some games don't seem to detect controllers, however SteamVR seems to still detect controllers.
- If you have **both** the non-Flatpak Steam and Flatpak Steam, trying to launch games with SteamVR seems to favour opening them in Flatpak Steam. Games opened with Flatpak Steam while running the non-Flatpak SteamVR don't appear on the headset.
---

## 1. Setting up SteamVR

So far, I only managed to get it working using the **non-flatpak** version of Steam.

1. Install `SteamVR` on the **non-Flatpak** version of `Steam`
2. Try to run `SteamVR`

Now, if SteamVR starts up saying something along the lines of *"SteamVR requires superuser access to finish setup"* Do the following:

1. Install `setcap` and `getcap`
- OpenSUSE Tumbleweed: `sudo zypper in libcap-progs`
- Fedora: `sudo dnf install libcap`
- Arch: `sudo pacman -S libcap`
2. Execute the following command: `sudo setcap CAP_SYS_NICE+ep ~/.local/share/Steam/steamapps/common/SteamVR/bin/linux64/vrcompositor-launcher`
3. Re-open SteamVR

---
## 2. Install the required udev rules

Udev Rules are required to allow programs to talk to the USB devices directly (The headset and the sensors)
On some distributions, you can simply install the `xr-hardware` package to get all the necessary Udev rules. For more information see [this guide](https://github.com/OpenHMD/OpenHMD/wiki/Udev-rules-list)

For the following distributions, you can simply install xr-hardware:
- OpenSUSE Tumbleweed: `opi xr-hardware`
- Arch: `yay -S xr-hardware`
---
## 3. Setting up the right version of OpenHMD

At the time of writing, `controller-haptics-wip` is the most up-to-date branch. For the sake of future-proofing, you can look up which branches are more updated on [Thaytan's github](https://github.com/thaytan/SteamVR-OpenHMD/branches/all)

1. `git` Clone the right branch of OpenHMD using the following command: 
`git clone --recursive --branch=controller-haptics-wip https://github.com/thaytan/SteamVR-OpenHMD/`

2. Once cloned, enter the directory with `cd SteamVR-OpenHMD`

3. Install the following required dependencies to compile hidapi, libusb, opencv (3.x or 4.x), libjpeg (libjpeg-turbo recommended) and meson
- Opensuse TW: `sudo zypper in meson libhidapi-devel libusb-devel opencv-devel libjpeg8-devel`
- Fedora: `sudo dnf install meson hidapi-devel libusb-devel opencv-devel libjpeg-turbo-devel`
- Arch: `sudo pacman -S meson hidapi libusb opencv libjpeg-turbo`
---
## 4. Compiling and installing

Now that everything required is installed, use `meson` and `ninja` to compile the `release` version of openHMD.

```bash
meson -Dbuildtype=release build
ninja -C build
```

Carefully read the output of the following command, any errors will cause the installation to fail.

```bash
./register.sh
```

A normal output should look something like this
```log
Found Steam in /home/user/.local/share/Steam/
Found SteamVR in /home/user/.local/share/Steam//steamapps/common/SteamVR
Installed linux plugin!
Did not find Windows plugin in build/ - not installing
Found config in /home/user/.local/share/Steam/config/steamvr.vrsettings
Backing up current config to /home/user/SteamVR-OpenHMD/steamvr-config-backup...
Backed up config!
Installing SteamVR-OpenHMD config...
Installed SteamVR-OpenHMD config
Registering SteamVR-OpenHMD plugin with SteamVR...
Registered SteamVR-OpenHMD plugin with SteamVR
```

---
## 5. Extra steps // Troubleshooting

On **OpenSUSE Tumbleweed** and some other distributions, at the time of writing, the shared library `libhidapi.so` is missing, so this extra step is necessary:
- `sudo cp build/subprojects/hidapi/libhidapi.so /usr/lib/`

Another extra step necessary on **OpenSUSE Tumbleweed** and some other distributions, is to make a symlink to `/usr/bin` for `setcap` and `getcap` since OpenHMD expects them at a specific path. You can do this with the following command:
- `sudo ln -s /usr/sbin/setcap /usr/bin && sudo ln -s /usr/sbin/getcap /usr/bin` 

### Testing to see if OpenHMD is working
OpenHMD comes with a built it example to test if the headset and sensors are recognized. To try it, plug everything and run the following:

- `./build/subprojects/openhmd/openhmd_simple_example`

### Troubleshooting SteamVR

The logs for SteamVR are located at these paths
- Setup: `/tmp/SteamVRLauncherSetup.log`
- Runtime: `~/.local/share/Steam/logs/vrserver.txt`

---
## Final words
After all this, you should be able to simply plug in your headset and launch SteamVR. I'm open to contributions, no matter how small!

